module gitlab.com/birowo/kursus-golang/nethttp

go 1.21.4

require (
	github.com/andybalholm/brotli v1.1.0
	github.com/fsnotify/fsnotify v1.7.0
	github.com/julienschmidt/httprouter v1.3.0
)

require golang.org/x/sys v0.4.0 // indirect
