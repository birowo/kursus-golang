package main

import (
	"github.com/julienschmidt/httprouter"
)

func router() (router *httprouter.Router) {
	router = httprouter.New()
	router.GET("/", static("root.html", "text/html; charset=utf-8"))
	return
}
