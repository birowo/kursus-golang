package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/andybalholm/brotli"
	"github.com/fsnotify/fsnotify"
	"github.com/julienschmidt/httprouter"
)

const (
	staticDir = "static/"
	ct        = "Content-Type"
	ce        = "Content-Encoding"
	lm        = "Last-Modified"
	ims       = "If-Modified-Since"
)

func filePool(name string) (pool sync.Pool) {
	pool = sync.Pool{
		New: func() any {
			f, err := os.Open(name)
			if err != nil {
				log.Fatalln(err)
			}
			return f
		},
	}
	return
}
func static(name, typ string) httprouter.Handle {
	lokal, _ := time.LoadLocation("")
	pool := filePool(staticDir + "br/" + name + ".br")
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		var (
			imsStr []string
			imsTm  time.Time
		)
		if imsStr = r.Header[ims]; len(imsStr) == 1 {
			var err error
			imsTm, err = time.Parse(http.TimeFormat, imsStr[0])
			if err != nil {
				log.Println(err)
				return
			}
		}
		f_ := pool.Get()
		defer pool.Put(f_)
		f := f_.(*os.File)
		info, _ := f.Stat()
		modTm := info.ModTime().In(lokal)
		if len(imsStr) != 1 || modTm.Unix() > imsTm.Unix() {
			wHdr := w.Header()
			wHdr[ct] = append(wHdr[ct], typ)
			wHdr[ce] = append(wHdr[ce], "br")
			wHdr[lm] = append(wHdr[lm], modTm.Format(http.TimeFormat))
			io.Copy(w, f)
			f.Seek(0, 0)
			return
		}
		w.WriteHeader(http.StatusNotModified)
	}
}
func brFile(dst, src string) {
	dst_, err := os.Create(dst)
	if err != nil {
		log.Fatalln(err)
	}
	writer := brotli.NewWriterV2(dst_, brotli.BestCompression)
	src_, err := os.Open(src)
	if err != nil {
		log.Fatalln(err)
	}
	io.Copy(writer, src_)
	writer.Close()
	dst_.Close()
	src_.Close()
}
func staticWatch() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Has(fsnotify.Create) || event.Has(fsnotify.Write) {
					//log.Println("create || write :", event.Name)
					brFile(staticDir+"br/"+event.Name[len(staticDir):]+".br", event.Name)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()
	err = watcher.Add(staticDir)
	if err != nil {
		log.Fatal(err)
	}
	<-make(chan struct{})
}
